FROM docker:23.0.5

ENV PYTHONUNBUFFERED=1

ARG ANSIBLE_VERSION
ARG MOLECULE_VERSION
ARG MOLECULE_DOCKER_VERSION

ENV ANSIBLE_VERSION=${ANSIBLE_VERSION}
ENV MOLECULE_VERSION=${MOLECULE_VERSION}
ENV MOLECULE_DOCKER_VERSION=${MOLECULE_DOCKER_VERSION}

RUN apk add --update --no-cache python3 rsync && ln -sf python3 /usr/bin/python && \
    python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools

RUN pip3 install \
    ansible==${ANSIBLE_VERSION} \
    molecule==${MOLECULE_VERSION} \
    molecule-docker==${MOLECULE_DOCKER_VERSION} && \
    pip3 cache purge
